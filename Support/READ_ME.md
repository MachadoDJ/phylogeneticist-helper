# PHCOMPARE

## Before you begin

The script named `phcompare.py` takes in two trees in separate files.

The trees must be rooted in the same way and they must be written in Newick format.

The support values in each note must be written aftern colons (`:`) as in `(A:1, (B:2, (C:3, D:4):5):6);`.

## Usage:

```
phcompare.py [-h] [-t1 TREE1] [-t2 TREE2]

optional arguments:
  -h, --help            show this help message and exit
  -t1 TREE1, --tree1 TREE1
                        Path to the first tree file in Newick format
  -t2 TREE2, --tree2 TREE2
                        Path to the second tree files in Newick format
```

## Example

These examples can be executed using files that come with `phcompare.py` in the `phylogenetics-helper` project (available from https://gitlab.com/MachadoDJ/phylogeneticist-helper).

### Example 1

`python3 phcompare.py -t1 example_trees/goodman-bremer-1.nwk -t2 example_trees/goodman-bremer-2.nwk > goodman-bremer.csv`

### Example 2

`python3 phcompare.py -t1 example_trees/REP-1.nwk -t2 example_trees/REP-2.nwk > REP.csv`