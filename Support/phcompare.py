#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pava.py
# Read several trees and compare the support values for the clades they share. Modified trees with clade numbers will be printed in the same location as the original trees and will received the ".modified" suffix.

##
# Import libraries
##

import argparse, sys, re
from Bio import SeqIO
from Bio.Seq import Seq

##
# Define functions
##

def stdout(text):
	return sys.stdout.write("{}\n".format(str(text)))

def stderr(text):
	return sys.stderr.write("{}\n".format(str(text)))

def _READ_TREE_(treeFile):
	tree = ""
	handle = open(treeFile, "r")
	tree = handle.read()
	handle.close()
	tree = re.compile("([^;]+;)", re.M|re.S).findall(tree)[0]
	tree = re.sub("[\r\n\s\t \'\"]+", "", tree)
	tree = re.sub(r"\)[\s ]*;", "):0.00;", tree)
	return tree

def _PARSE_TREE_1_(tree, path):
	original_tree = tree
	cladesDic     = {}
	codes         = {}
	cladeCount    = 0
	while True:
		smaller_clades = re.compile("(\([^\(\)]+\)[^,\(\);]*)").findall(tree)
		if not smaller_clades:
			break
		else:
			for clade in smaller_clades:
				try:
					support = float(re.compile("\)[^:\(\)\[\].;]*:(.+)").findall(clade)[0])
				except:
					stderr("!!!ERROR: Could not find the support value or could not convert it into a float. See clade below:\n{}".format(clade))
					exit()
				else:
					cladeCount     += 1
					terminals       = [terminal for terminal in [re.sub(r":.*", "", terminal) for terminal in re.compile("([^\(\)\[\],;]+)").findall(re.sub(r"([\]\)])\d+", r"\1", clade))] if terminal]
					name            = ",".join(sorted(terminals))
					codes[name]     = cladeCount
					cladesDic[name] = support
					modClade        = re.sub(r"\).+", ")", clade)
					modClade        = modClade.replace("(", "[")
					modClade        = modClade.replace(")", "]")
					modClade        = "{}{}:{}".format(modClade, cladeCount, support)
					tree = tree.replace(clade, modClade)
	tree   = tree.replace("[", "(")
	tree   = tree.replace("]", ")")
	handle = open("{}.modified".format(path), "w")
	handle.write(tree)
	handle.close()
	return cladesDic, codes

def _PARSE_TREE_2_(tree, path, codes):
	original_tree = tree
	cladesDic     = {}
	while True:
		smaller_clades = re.compile("(\([^\(\)]+\)[^,\(\);]*)").findall(tree)
		if not smaller_clades:
			break
		else:
			for clade in smaller_clades:
				try:
					support = float(re.compile("\)[^:\(\)\[\].;]*:(.+)").findall(clade)[0])
				except:
					stderr("!!!ERROR: Could not find the support value or could not convert it into a float. See clade below:\n{}".format(clade))
					exit()
				else:
					terminals       = [terminal for terminal in [re.sub(r":.*", "", terminal) for terminal in re.compile("([^\(\)\[\],;]+)").findall(re.sub(r"([\]\)])\d+", r"\1", clade))] if terminal]
					name            = ",".join(sorted(terminals))
					if name in codes:
						cladesDic[name] = support
						modClade        = re.sub(r"\).+", ")", clade)
						modClade        = modClade.replace("(", "[")
						modClade        = modClade.replace(")", "]")
						modClade        = "{}{}:{}".format(modClade, codes[name], support)
						tree = tree.replace(clade, modClade)
					else:
						modClade        = re.sub(r"\).+", ")", clade)
						modClade        = modClade.replace("(", "[")
						modClade        = modClade.replace(")", "]")
						modClade        = "{}:{}".format(modClade, support)
						tree = tree.replace(clade, modClade)
	tree   = tree.replace("[", "(")
	tree   = tree.replace("]", ")")
	handle = open("{}.modified".format(path), "w")
	handle.write(tree)
	handle.close()
	return cladesDic

def _COMPARE_CLADE_SUPPORT_(clades1, clades2, codes):
	stdout("Node,Tree1,Tree2")
	for clade in codes:
		code     = codes[clade]
		support1 = clades1[clade]
		support2 = clades2[clade]
		stdout("{},{},{}".format(code, support1, support2))
	return

def _MAIN_():
	tree1          = _READ_TREE_(args.tree1)
	tree2          = _READ_TREE_(args.tree2)
	clades1, codes = _PARSE_TREE_1_(tree1, args.tree1)
	clades2        = _PARSE_TREE_2_(tree2, args.tree2, codes)
	for clade in clades1:
		if not clade in clades2:
			del codes[clade]
	_COMPARE_CLADE_SUPPORT_(clades1, clades2, codes)
	return

##
# Execute functions
##

if __name__ == '__main__':
	parser=argparse.ArgumentParser()
	parser.add_argument("-t1", "--tree1", help = "Path to the first tree file in Newick format",   type = str)
	parser.add_argument("-t2", "--tree2", help = "Path to the second tree files in Newick format", type = str)
	args = parser.parse_args()
	_MAIN_() # This is the main fuction

exit() # Quit this script
