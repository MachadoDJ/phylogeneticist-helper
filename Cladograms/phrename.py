#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# phrename.py
# Given a file containing trees and another file containing tab-separated taxon names and synonyms, substitute names by synonyms or vice-versa.

# By Denis Jacob Machado on April 19, 2018
# This algorithm is case sensitive

# Import modules and libraries
import argparse, re, sys

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i", "--input", help = "Input file with the trees that will be renamed", type = str, required = True)
parser.add_argument("-r", "--reverse", help = "Reverse substitution order (column 1 -> 2 becomes column 2 -> 1)", action = "store_true", default = False, required = False)
parser.add_argument("-s", "--synonyms", help = "Synonyms file (2 columns, tab-separated values)", type = str, required = True)
args=parser.parse_args()

def get_synonyms(): # Reads synonyms files and store data into a dictionary
	syn_dic = {}
	handle  = open(args.synonyms, "r")
	if(args.reverse):
		for x, y in re.compile("(.+)\t(.+)").findall(handle.read()):
			syn_dic["{}".format(x.strip())] = y.strip()
	else:
		for x, y in re.compile("(.+)\t(.+)").findall(handle.read()):
			syn_dic["{}".format(y.strip())] = x.strip()
	handle.close()
	return syn_dic

def replace_names(syn_dic): # Replace taxon names
	handle = open(args.input, "r")
	data = handle.read()
	handle.close()
	for key in sorted(syn_dic, key=len, reverse=True):
		data  = data.replace(key, syn_dic[key])
	sys.stdout.write(data) # Results will be printed on the STDOUT
	return

syn_dic = get_synonyms()
replace_names(syn_dic)

exit()
