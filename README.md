# The phylogeneticist helper

Set of scripts to assist on operations related to the practice of phylogenetic computation.

# Author

Denis JACOB MACHADO, Ph.D.

Email: machadodj <at> alumni.usp.br.

# License

All contents are available under GNU GENERAL PUBLIC LICENSE (Version 3, 29 June 2007).

The following applies to all software herein:

>>>
    These programs are free software: you can redistribute and/or modify
    them under the terms of the GNU General Public License version 3
    as published by the Free Software Foundation.

    These programs are distributed in the hope that they will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    There is a copy of the GNU General Public License herein. Also,
    see <https://www.gnu.org/licenses/>.
>>>

# How to use the programs herein

All programs herein have a "help" option. Simply execute the program with "-h" or "--help" in front of it for details.

For example:

```python3 <program> --help```