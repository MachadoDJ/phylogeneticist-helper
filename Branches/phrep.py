#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# phrep.py
# Given a tree in Nexus format containing support values (indicated as
# [&support=X]), the cost of the best tree, and the cost of the worst tree,
# modigy the given three to replace Goodman-Bremer values for REP support
# values.

# Import modules and libraries
import argparse, sys, re

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-t", "--tree", help="Tree in Nexus format", type = str, required = True)
parser.add_argument("-b", "--best", help="Length of the most-parsimonious tree", type = float, required = True)
parser.add_argument("-w", "--worst", help="Length of the least-parsimonious tree", type = float, required = True)
parser.add_argument("-c", "--constant", help="Constant value to be multiplied by REP (default = 1.0)", type = float, default = 1.0, required = False)
args=parser.parse_args()

# Define functions
def read_tree():
	handle = open(args.tree, "r") # Open the tree in read-only mode
	data = handle.read() # Stode text into variable
	handle.close() # Close file
	try: # This "try" helps in case the file is not formatted as expected
		begin, tree, end = re.compile("(.+begin trees\s*;.+?tree.+?=.+?)(\(.+?;)(.+?end\s*;.*)",re.I|re.M|re.S).findall(data)[0]
	except:
		sys.stderr.write("¡ERROR! Could not read tree from file.\n") # In case of error, see if input tree looks like the example
		exit()
	else:
		tree = re.sub("\s", "", tree)
		tree = re.sub("SUPPORT", "support", tree, flags=re.I) # The idea is to get several substrings, separated on the support value. Them, we just need to edit each one of them and glue them back together.
		tree = tree.split("support=")
	for i in range(0, len(tree)):
		subtree = tree[i]
		try:
			gb, tail = re.compile("^(\d+)(.+)").findall(subtree)[0] # I am assuming Goodman-Bremer values are integers
		except:
			pass
		else:
			rep = (float(gb)/ (args.worst - args.best)) * args.constant
			tree[i] = "{}{}".format(rep, tail)
	sys.stdout.write("{}{}{}\n".format(begin, "support=".join(tree), end))
	return


# Execute functions
read_tree()


# Quit
exit()